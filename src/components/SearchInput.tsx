import SearchIcon from "@mui/icons-material/Search";
import {
    FormControl,
    InputAdornment,
    FilledInput,
    Icon,
} from "@mui/material";
import { ChangeEvent } from "react";

interface Props {
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const SearchInput: Component<Props> = ({ onChange }) => {
    return (
        <FormControl sx={{ m: 2, width: "25ch" }} variant="outlined">
            <FilledInput
                type="text"
                placeholder="Search for teams, players, matches..."
                startAdornment={
                    <InputAdornment position="start">
                        <Icon>
                            <SearchIcon color="info" />
                        </Icon>
                    </InputAdornment>
                }
                sx={{
                    width: 350,
                    color: "#FFF",
                }}
                onChange={onChange}
            />
        </FormControl>
    );
};
