import {
    Paper,
    TableContainer,
    TableHead,
    TableRow,
    Table,
    TableCell,
    TableBody,
    CircularProgress,
    TableFooter,
    TablePagination
} from "@mui/material";
import { useState, useEffect } from "react";
import { CountryFlag } from "./CountryFlag";

interface Props {
    searchTerm: string;
}

export const Leaderboard: Component<Props> = ({ searchTerm }) => {
    const [leaderboardData, setLeaderboardData] = useState<
        leaderboardResponseData | undefined
    >();
    const [page, setPage] = useState(0);
    const currentYear = new Date().getFullYear();

    const onPageChange = (page: number) => {
        setPage(page);
    };

    useEffect(() => {
        setLeaderboardData(undefined);

        const fetchLeaderboardData = async () => {
            let queryParams;
            if (searchTerm) {
                queryParams = `?searchBy=${searchTerm}${
                    page > 0 ? `&page=${page}` : ""
                }`;
            } else {
                queryParams = `?page=${page}`;
            }

            try {
                const data = await fetch(
                    `https://api.ggpredict.dev:8080/restapi/players${queryParams}`
                );
                const json = await data.json();

                setLeaderboardData(json);
            } catch (error) {
                console.error(error);
            }
        };

        fetchLeaderboardData();
    }, [searchTerm, page]);

    useEffect(() => {
        setPage(0);
    }, [searchTerm]);

    return (
        <TableContainer component={Paper}>
            {leaderboardData ? (
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Nick</TableCell>
                            <TableCell>Age</TableCell>
                            <TableCell align="center">Country</TableCell>
                            <TableCell align="center">Won</TableCell>
                            <TableCell align="center">Drawn</TableCell>
                            <TableCell align="center">Lost</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {leaderboardData.content.map((player: player) => {
                            const {
                                id,
                                name,
                                nick,
                                birthYear,
                                country: countryCode,
                            } = player;

                            return (
                                <TableRow key={id}>
                                    <TableCell>{name}</TableCell>
                                    <TableCell>{nick}</TableCell>
                                    <TableCell>
                                        {currentYear - birthYear}
                                    </TableCell>
                                    <TableCell align="center">
                                        <CountryFlag
                                            src={`https://countryflagsapi.com/png/${countryCode}`}
                                            alt={countryCode}
                                        />
                                    </TableCell>
                                    <TableCell align="center">0</TableCell>
                                    <TableCell align="center">0</TableCell>
                                    <TableCell align="center">0</TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TableCell
                                sx={{
                                    display: "flex",
                                    height: "100%",
                                    fontSize: "14px",
                                    color: "black",
                                }}
                            >
                                <p style={{ width: "100px" }}>
                                    {leaderboardData.numberOfElements} per page
                                </p>
                                <p style={{ width: "100px" }}>
                                    Page{" "}
                                    {leaderboardData.pageable.pageNumber + 1} of{" "}
                                    {leaderboardData.totalPages}
                                </p>
                            </TableCell>
                            <TablePagination
                                count={leaderboardData.totalElements}
                                rowsPerPage={20}
                                rowsPerPageOptions={[-1]}
                                page={leaderboardData.pageable.pageNumber}
                                onPageChange={(_, page) => onPageChange(page)}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            ) : (
                <CircularProgress />
            )}
        </TableContainer>
    );
};
