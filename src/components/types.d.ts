interface player {
    id: number;
    name: string;
    nick: string;
    surname: string;
    birthYear: number;
    country: string;
    photo: string;
    bodyshot: string;
    team: team;
    ratingValue: number;
    rankingPosition: number;
    lastRatingValue: number;
    rating: Array;
}

interface team {
    id: number;
    name: string;
    logo: string;
    coutnry: string;
    winRatio: number;
    ratingValue: number;
    rankingPosition: number;
    lastRatingValue: number;
    lastRankingPosition: number;
    lastResults: string[];
    rating: Array;
}

interface sortingData {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}

interface pageable {
    sort: sortingData;
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    unpaged: boolean;
}

interface responseMetadata {
    totalElements: number;
    totalPages: number;
    last: boolean;
    size: number;
    number: number;
    sort: sortingData;
    first: boolean;
    numberOfElements: number;
    empty: boolean;
}

// type leaderboardResponseData = player & pageable & responseMetadata;

type leaderboardResponseData = {
    content: player[];
    pageable: pageable;
} & responseMetadata;
