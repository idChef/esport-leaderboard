import styled from "@emotion/styled";

export const CountryFlag = styled.img`
    width: 32px;
    height: 20px;
`;
