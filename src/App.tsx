import React, { ChangeEvent, useMemo, useState } from "react";
import "./App.css";
import { SearchInput } from "./components/SearchInput";
import { Leaderboard } from "./components/Leaderboard";
//@ts-ignore
import debounce from "lodash.debounce";

function App() {
    const [searchTerm, setSearchTerm] = useState("");

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(e.target.value);
    };

    const debouncedInputChange = useMemo(
        () => debounce(onInputChange, 300),
        []
    );

    return (
        <div className="App">
            <SearchInput onChange={debouncedInputChange} />
            <Leaderboard searchTerm={searchTerm} />
        </div>
    );
}

export default App;
