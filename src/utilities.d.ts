export {};

declare global {
  export type Component<T = unknown> = React.FC<T>;

  export type IfEquals<X, Y, A = X, B = never> = (<T>() => T extends X ? 1 : 2) extends <T>() => T extends Y ? 1 : 2 ? A : B;

  export type ReadonlyKeys<T> = {
    [P in keyof T]-?: IfEquals<{ [Q in P]: T[P] }, { -readonly [Q in P]: T[P] }, never, P>;
  }[keyof T];

  export type MutableOnly<T> = Omit<T, ReadonlyKeys<T>>;

  // It's from craftjs package
  export declare type Delete<T, U> = Pick<T, Exclude<keyof T, U>>;
}
